<?php
/**
 * Copyright (C) $user$, Inc - All Rights Reserved
 *
 *  <other text>
 * @file        Task.php
 * @author      ignatenkovnikita
 * @date        $date$
 */

/**
 * Created by PhpStorm.
 * User: ignatenkovnikita
 * Web Site: http://IgnatenkovNikita.ru
 */

namespace frontend\modules\api\v1\resources;


class Task extends \common\models\Task
{

    public function fields()
    {
        return [
            'id',
            'name',
            'description',
            'status' => function () {
                return [
                    'id' => $this->status_id,
                    'name' => $this->status->name
                ];
            },
            'deadline',
            'created_at',
            'updated_at',


        ];
    }
}