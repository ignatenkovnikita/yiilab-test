<?php
/**
 * Copyright (C) $user$, Inc - All Rights Reserved
 *
 *  <other text>
 * @file        TaskStatus.php
 * @author      ignatenkovnikita
 * @date        $date$
 */

/**
 * Created by PhpStorm.
 * User: ignatenkovnikita
 * Web Site: http://IgnatenkovNikita.ru
 */

namespace frontend\modules\api\v1\resources;


class TaskStatus extends \common\models\TaskStatus
{


    public function fields()
    {
        return [
            'id',
            'name'
        ];
    }
}