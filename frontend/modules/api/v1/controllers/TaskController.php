<?php
/**
 * Copyright (C) $user$, Inc - All Rights Reserved
 *
 *  <other text>
 * @file        TaskController.php
 * @author      ignatenkovnikita
 * @date        $date$
 */

/**
 * Created by PhpStorm.
 * User: ignatenkovnikita
 * Web Site: http://IgnatenkovNikita.ru
 */

namespace frontend\modules\api\v1\controllers;


use common\models\TaskStatus;
use frontend\models\search\TaskSearch;
use frontend\modules\api\v1\resources\Task;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

class TaskController extends ActiveController
{

    public $modelClass = Task::class;
    /**
     * @var array
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function actions() {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }
    public function prepareDataProvider() {
        $searchModel = new TaskSearch();
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}