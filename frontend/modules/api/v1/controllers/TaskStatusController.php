<?php
/**
 * Copyright (C) $user$, Inc - All Rights Reserved
 *
 *  <other text>
 * @file        TaskStatusController.php
 * @author      ignatenkovnikita
 * @date        $date$
 */

/**
 * Created by PhpStorm.
 * User: ignatenkovnikita
 * Web Site: http://IgnatenkovNikita.ru
 */

namespace frontend\modules\api\v1\controllers;


use frontend\modules\api\v1\resources\TaskStatus;
use yii\rest\ActiveController;

class TaskStatusController extends ActiveController
{
    public $modelClass = TaskStatus::class;

}