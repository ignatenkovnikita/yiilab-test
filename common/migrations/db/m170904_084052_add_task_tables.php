<?php

use yii\db\Migration;

class m170904_084052_add_task_tables extends Migration
{

    use \ignatenkovnikita\migrationsaddons\AddCreatedUpdated;
    use \ignatenkovnikita\migrationsaddons\AddAuthorUpdater;
    use \ignatenkovnikita\migrationsaddons\ForeignKeyTrait;

    const NAME_TASK = '{{%task}}';
    const NAME_TASK_STATUS = '{{%task_status}}';

    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::NAME_TASK, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'status_id' => $this->integer()->notNull(),
            'deadline' => $this->bigInteger()
        ], $tableOptions);
        $this->addAllTime(self::NAME_TASK);
        $this->addAllUser(self::NAME_TASK);

        $this->createTable(self::NAME_TASK_STATUS, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'sort' => $this->integer()
        ], $tableOptions);
        $this->addAllTime(self::NAME_TASK_STATUS);
        $this->addAllUser(self::NAME_TASK_STATUS);


        $this->addForeignKeys(self::NAME_TASK, [
            ['status_id', self::NAME_TASK_STATUS, 'id']
        ]);


    }

    public function safeDown()
    {
        $this->dropForeignKeys(self::NAME_TASK, [
            ['status_id', self::NAME_TASK_STATUS, 'id']
        ]);

        $this->dropTable(self::NAME_TASK_STATUS);
        $this->dropTable(self::NAME_TASK);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170904_084052_add_task_tables cannot be reverted.\n";

        return false;
    }
    */
}
