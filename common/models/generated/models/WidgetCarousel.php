<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "widget_carousel".
 *
 * @property integer $id ID
 * @property string $key Key
 * @property integer $status Status
 *
     * @property WidgetCarouselItem[] $widgetCarouselItems
    */
class WidgetCarousel extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

            
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'widget_carousel';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['status'], 'integer'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'status' => 'Status',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getWidgetCarouselItems()
    {
        return $this->hasMany($this->called_class_namespace . '\WidgetCarouselItem', ['carousel_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\WidgetCarouselQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\WidgetCarouselQuery(get_called_class());
    }
}
