<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "widget_menu".
 *
 * @property integer $id ID
 * @property string $key Key
 * @property string $title Title
 * @property string $items Items
 * @property integer $status Status
*/
class WidgetMenu extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                    
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'widget_menu';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['key', 'title', 'items'], 'required'],
            [['items'], 'string'],
            [['status'], 'integer'],
            [['key'], 'string', 'max' => 32],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'title' => 'Title',
            'items' => 'Items',
            'status' => 'Status',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\WidgetMenuQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\WidgetMenuQuery(get_called_class());
    }
}
