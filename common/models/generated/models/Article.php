<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id ID
 * @property string $slug Slug
 * @property string $title Title
 * @property string $body Body
 * @property string $view View
 * @property integer $category_id Category ID
 * @property string $thumbnail_base_url Thumbnail Base Url
 * @property string $thumbnail_path Thumbnail Path
 * @property integer $status Status
 * @property integer $created_by Created By
 * @property integer $updated_by Updated By
 * @property integer $published_at Published At
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 *
     * @property User $createdBy
     * @property ArticleCategory $category
     * @property User $updatedBy
     * @property ArticleAttachment[] $articleAttachments
    */
class Article extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                                        
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'author' => \yii\behaviors\BlameableBehavior::class,
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'article';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['slug', 'title', 'body'], 'required'],
            [['body'], 'string'],
            [['category_id', 'status', 'created_by', 'updated_by', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['slug', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['title'], 'string', 'max' => 512],
            [['view'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Title',
            'body' => 'Body',
            'view' => 'View',
            'category_id' => 'Category ID',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'status' => 'Status',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'published_at' => 'Published At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getCreatedBy()
    {
        return $this->hasOne($this->called_class_namespace . '\User', ['id' => 'created_by']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getCategory()
    {
        return $this->hasOne($this->called_class_namespace . '\ArticleCategory', ['id' => 'category_id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getUpdatedBy()
    {
        return $this->hasOne($this->called_class_namespace . '\User', ['id' => 'updated_by']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticleAttachments()
    {
        return $this->hasMany($this->called_class_namespace . '\ArticleAttachment', ['article_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\ArticleQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\ArticleQuery(get_called_class());
    }
}
