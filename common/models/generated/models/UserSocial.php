<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "user_social".
 *
 * @property integer $id ID
 * @property integer $user_id User ID
 * @property string $oauth_client Oauth Client
 * @property string $oauth_client_user_id Oauth Client User ID
 *
     * @property User $user
    */
class UserSocial extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'user_social';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['oauth_client', 'oauth_client_user_id'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'oauth_client' => 'Oauth Client',
            'oauth_client_user_id' => 'Oauth Client User ID',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getUser()
    {
        return $this->hasOne($this->called_class_namespace . '\User', ['id' => 'user_id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\UserSocialQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\UserSocialQuery(get_called_class());
    }
}
