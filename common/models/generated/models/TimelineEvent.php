<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "timeline_event".
 *
 * @property integer $id ID
 * @property string $application Application
 * @property string $category Category
 * @property string $event Event
 * @property string $data Data
 * @property integer $created_at Created At
*/
class TimelineEvent extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                        
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'timeline_event';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['application', 'category', 'event', 'created_at'], 'required'],
            [['data'], 'string'],
            [['created_at'], 'integer'],
            [['application', 'category', 'event'], 'string', 'max' => 64],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application' => 'Application',
            'category' => 'Category',
            'event' => 'Event',
            'data' => 'Data',
            'created_at' => 'Created At',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\TimelineEventQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\TimelineEventQuery(get_called_class());
    }
}
