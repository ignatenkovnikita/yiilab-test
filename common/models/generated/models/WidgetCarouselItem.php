<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "widget_carousel_item".
 *
 * @property integer $id ID
 * @property integer $carousel_id Carousel ID
 * @property string $base_url Base Url
 * @property string $path Path
 * @property string $type Type
 * @property string $url Url
 * @property string $caption Caption
 * @property integer $status Status
 * @property integer $order Order
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 *
     * @property WidgetCarousel $carousel
    */
class WidgetCarouselItem extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                            
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'widget_carousel_item';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['carousel_id'], 'required'],
            [['carousel_id', 'status', 'order', 'created_at', 'updated_at'], 'integer'],
            [['base_url', 'path', 'url', 'caption'], 'string', 'max' => 1024],
            [['type'], 'string', 'max' => 255],
            [['carousel_id'], 'exist', 'skipOnError' => true, 'targetClass' => WidgetCarousel::className(), 'targetAttribute' => ['carousel_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'carousel_id' => 'Carousel ID',
            'base_url' => 'Base Url',
            'path' => 'Path',
            'type' => 'Type',
            'url' => 'Url',
            'caption' => 'Caption',
            'status' => 'Status',
            'order' => 'Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getCarousel()
    {
        return $this->hasOne($this->called_class_namespace . '\WidgetCarousel', ['id' => 'carousel_id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\WidgetCarouselItemQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\WidgetCarouselItemQuery(get_called_class());
    }
}
