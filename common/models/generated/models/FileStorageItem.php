<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "file_storage_item".
 *
 * @property integer $id ID
 * @property string $component Component
 * @property string $base_url Base Url
 * @property string $path Path
 * @property string $type Type
 * @property integer $size Size
 * @property string $name Name
 * @property string $upload_ip Upload Ip
 * @property integer $created_at Created At
*/
class FileStorageItem extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                    
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'file_storage_item';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['component', 'base_url', 'path', 'created_at'], 'required'],
            [['size', 'created_at'], 'integer'],
            [['component', 'type', 'name'], 'string', 'max' => 255],
            [['base_url', 'path'], 'string', 'max' => 1024],
            [['upload_ip'], 'string', 'max' => 15],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'component' => 'Component',
            'base_url' => 'Base Url',
            'path' => 'Path',
            'type' => 'Type',
            'size' => 'Size',
            'name' => 'Name',
            'upload_ip' => 'Upload Ip',
            'created_at' => 'Created At',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\FileStorageItemQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\FileStorageItemQuery(get_called_class());
    }
}
