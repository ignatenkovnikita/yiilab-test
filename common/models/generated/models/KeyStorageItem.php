<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property string $key Key
 * @property string $value Value
 * @property string $comment Comment
 * @property integer $updated_at Updated At
 * @property integer $created_at Created At
*/
class KeyStorageItem extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                    
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'key_storage_item';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['value', 'comment'], 'string'],
            [['updated_at', 'created_at'], 'integer'],
            [['key'], 'string', 'max' => 128],
            [['key'], 'unique'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'key' => 'Key',
            'value' => 'Value',
            'comment' => 'Comment',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\KeyStorageItemQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\KeyStorageItemQuery(get_called_class());
    }
}
