<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "data_list".
 *
 * @property integer $id ID
 * @property string $type Type
 * @property string $name Name
 * @property integer $position Position
*/
class DataList extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'data_list';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['type', 'name'], 'required'],
            [['position'], 'integer'],
            [['type'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'position' => 'Position',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\DataListQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\DataListQuery(get_called_class());
    }
}
