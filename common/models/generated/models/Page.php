<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id ID
 * @property string $slug Slug
 * @property string $title Title
 * @property string $body Body
 * @property string $view View
 * @property integer $status Status
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
*/
class Page extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'page';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['slug', 'title', 'body', 'status'], 'required'],
            [['body'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['slug'], 'string', 'max' => 2048],
            [['title'], 'string', 'max' => 512],
            [['view'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Title',
            'body' => 'Body',
            'view' => 'View',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\PageQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\PageQuery(get_called_class());
    }
}
