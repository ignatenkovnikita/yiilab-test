<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id User ID
 * @property string $firstname Firstname
 * @property string $middlename Middlename
 * @property string $lastname Lastname
 * @property string $avatar_path Avatar Path
 * @property string $avatar_base_url Avatar Base Url
 * @property string $locale Locale
 * @property integer $gender Gender
 *
     * @property User $user
    */
class UserProfile extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['locale'], 'required'],
            [['gender'], 'integer'],
            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 32],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
            'avatar_path' => 'Avatar Path',
            'avatar_base_url' => 'Avatar Base Url',
            'locale' => 'Locale',
            'gender' => 'Gender',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getUser()
    {
        return $this->hasOne($this->called_class_namespace . '\User', ['id' => 'user_id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\UserProfileQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\UserProfileQuery(get_called_class());
    }
}
