<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "system_log".
 *
 * @property integer $id ID
 * @property integer $level Level
 * @property string $category Category
 * @property double $log_time Log Time
 * @property string $prefix Prefix
 * @property string $message Message
*/
class SystemLog extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                        
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'system_log';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['level'], 'integer'],
            [['log_time'], 'number'],
            [['prefix', 'message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'category' => 'Category',
            'log_time' => 'Log Time',
            'prefix' => 'Prefix',
            'message' => 'Message',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\SystemLogQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\SystemLogQuery(get_called_class());
    }
}
