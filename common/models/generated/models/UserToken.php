<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "user_token".
 *
 * @property integer $id ID
 * @property integer $user_id User ID
 * @property string $type Type
 * @property string $token Token
 * @property integer $expire_at Expire At
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
*/
class UserToken extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                            
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'user_token';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['user_id', 'type', 'token'], 'required'],
            [['user_id', 'expire_at', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'string', 'max' => 255],
            [['token'], 'string', 'max' => 40],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'token' => 'Token',
            'expire_at' => 'Expire At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\UserTokenQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\UserTokenQuery(get_called_class());
    }
}
