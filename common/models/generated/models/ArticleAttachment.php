<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "article_attachment".
 *
 * @property integer $id ID
 * @property integer $article_id Article ID
 * @property string $path Path
 * @property string $base_url Base Url
 * @property string $type Type
 * @property integer $size Size
 * @property string $name Name
 * @property integer $created_at Created At
 * @property integer $order Order
 *
     * @property Article $article
    */
class ArticleAttachment extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                    
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'article_attachment';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['article_id', 'path'], 'required'],
            [['article_id', 'size', 'created_at', 'order'], 'integer'],
            [['path', 'base_url', 'type', 'name'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'path' => 'Path',
            'base_url' => 'Base Url',
            'type' => 'Type',
            'size' => 'Size',
            'name' => 'Name',
            'created_at' => 'Created At',
            'order' => 'Order',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticle()
    {
        return $this->hasOne($this->called_class_namespace . '\Article', ['id' => 'article_id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\ArticleAttachmentQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\ArticleAttachmentQuery(get_called_class());
    }
}
