<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "rbac_auth_item_child".
 *
 * @property string $parent Parent
 * @property string $child Child
 *
     * @property RbacAuthItem $parent0
     * @property RbacAuthItem $child0
    */
class RbacAuthItemChild extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

        
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'rbac_auth_item_child';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthItem::className(), 'targetAttribute' => ['parent' => 'name']],
            [['child'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthItem::className(), 'targetAttribute' => ['child' => 'name']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getParent0()
    {
        return $this->hasOne($this->called_class_namespace . '\RbacAuthItem', ['name' => 'parent']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getChild0()
    {
        return $this->hasOne($this->called_class_namespace . '\RbacAuthItem', ['name' => 'child']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\RbacAuthItemChildQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\RbacAuthItemChildQuery(get_called_class());
    }
}
