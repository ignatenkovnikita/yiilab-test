<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id ID
 * @property string $username Username
 * @property string $auth_key Auth Key
 * @property string $access_token Access Token
 * @property string $password_hash Password Hash
 * @property string $oauth_client Oauth Client
 * @property string $oauth_client_user_id Oauth Client User ID
 * @property string $email Email
 * @property integer $status Status
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 * @property integer $logged_at Logged At
 *
     * @property Article[] $articles
     * @property Article[] $articles0
     * @property UserProfile $userProfile
     * @property UserSocial[] $userSocials
    */
class User extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                                
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'user';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['auth_key', 'access_token', 'password_hash', 'email'], 'required'],
            [['status', 'created_at', 'updated_at', 'logged_at'], 'integer'],
            [['username', 'auth_key'], 'string', 'max' => 32],
            [['access_token'], 'string', 'max' => 40],
            [['password_hash', 'oauth_client', 'oauth_client_user_id', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'password_hash' => 'Password Hash',
            'oauth_client' => 'Oauth Client',
            'oauth_client_user_id' => 'Oauth Client User ID',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'logged_at' => 'Logged At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticles()
    {
        return $this->hasMany($this->called_class_namespace . '\Article', ['created_by' => 'id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticles0()
    {
        return $this->hasMany($this->called_class_namespace . '\Article', ['updated_by' => 'id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getUserProfile()
    {
        return $this->hasOne($this->called_class_namespace . '\UserProfile', ['user_id' => 'id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getUserSocials()
    {
        return $this->hasMany($this->called_class_namespace . '\UserSocial', ['user_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\UserQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\UserQuery(get_called_class());
    }
}
