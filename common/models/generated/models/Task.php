<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id ID
 * @property string $name Name
 * @property string $description Description
 * @property integer $status_id Status ID
 * @property integer $deadline Deadline
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 * @property integer $created_by Created By
 * @property integer $updated_by Updated By
 *
     * @property TaskStatus $status
    */
class Task extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                    
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
            'author' => \yii\behaviors\BlameableBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'task';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['name', 'status_id'], 'required'],
            [['description'], 'string'],
            [['status_id', 'deadline', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'status_id' => 'Status ID',
            'deadline' => 'Deadline',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getStatus()
    {
        return $this->hasOne($this->called_class_namespace . '\TaskStatus', ['id' => 'status_id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\TaskQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\TaskQuery(get_called_class());
    }
}
