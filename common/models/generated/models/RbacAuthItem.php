<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "rbac_auth_item".
 *
 * @property string $name Name
 * @property integer $type Type
 * @property string $description Description
 * @property string $rule_name Rule Name
 * @property resource $data Data
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 *
     * @property RbacAuthAssignment[] $rbacAuthAssignments
     * @property RbacAuthRule $ruleName
     * @property RbacAuthItemChild[] $rbacAuthItemChildren
     * @property RbacAuthItemChild[] $rbacAuthItemChildren0
     * @property RbacAuthItem[] $children
     * @property RbacAuthItem[] $parents
    */
class RbacAuthItem extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                            
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'rbac_auth_item';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthRule::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getRbacAuthAssignments()
    {
        return $this->hasMany($this->called_class_namespace . '\RbacAuthAssignment', ['item_name' => 'name']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getRuleName()
    {
        return $this->hasOne($this->called_class_namespace . '\RbacAuthRule', ['name' => 'rule_name']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getRbacAuthItemChildren()
    {
        return $this->hasMany($this->called_class_namespace . '\RbacAuthItemChild', ['parent' => 'name']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getRbacAuthItemChildren0()
    {
        return $this->hasMany($this->called_class_namespace . '\RbacAuthItemChild', ['child' => 'name']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getChildren()
    {
        return $this->hasMany($this->called_class_namespace . '\RbacAuthItem', ['name' => 'child'])->viaTable('rbac_auth_item_child', ['parent' => 'name']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getParents()
    {
        return $this->hasMany($this->called_class_namespace . '\RbacAuthItem', ['name' => 'parent'])->viaTable('rbac_auth_item_child', ['child' => 'name']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\RbacAuthItemQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\RbacAuthItemQuery(get_called_class());
    }
}
