<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "i18n_source_message".
 *
 * @property integer $id ID
 * @property string $category Category
 * @property string $message Message
 *
     * @property I18nMessage[] $i18nMessages
    */
class I18nSourceMessage extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

            
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'i18n_source_message';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'message' => 'Message',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getI18nMessages()
    {
        return $this->hasMany($this->called_class_namespace . '\I18nMessage', ['id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\I18nSourceMessageQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\I18nSourceMessageQuery(get_called_class());
    }
}
