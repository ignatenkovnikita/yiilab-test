<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "rbac_auth_assignment".
 *
 * @property string $item_name Item Name
 * @property string $user_id User ID
 * @property integer $created_at Created At
 *
     * @property RbacAuthItem $itemName
    */
class RbacAuthAssignment extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

            
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'rbac_auth_assignment';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['created_at'], 'integer'],
            [['item_name', 'user_id'], 'string', 'max' => 64],
            [['item_name'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthItem::className(), 'targetAttribute' => ['item_name' => 'name']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'item_name' => 'Item Name',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getItemName()
    {
        return $this->hasOne($this->called_class_namespace . '\RbacAuthItem', ['name' => 'item_name']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\RbacAuthAssignmentQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\RbacAuthAssignmentQuery(get_called_class());
    }
}
