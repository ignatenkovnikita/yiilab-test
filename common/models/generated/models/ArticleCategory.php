<?php

namespace common\models\generated\models;

use Yii;

/**
 * This is the model class for table "article_category".
 *
 * @property integer $id ID
 * @property string $slug Slug
 * @property string $title Title
 * @property string $body Body
 * @property integer $parent_id Parent ID
 * @property integer $status Status
 * @property integer $created_at Created At
 * @property integer $updated_at Updated At
 *
     * @property Article[] $articles
     * @property ArticleCategory $parent
     * @property ArticleCategory[] $articleCategories
    */
class ArticleCategory extends \common\ActiveRecord
{
    private $called_class_namespace;

    public function __construct()
    {
        $this->called_class_namespace = substr(get_called_class(), 0, strrpos(get_called_class(), '\\'));
        parent::__construct();
    }

                                
    /**
     * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'article_category';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['slug', 'title'], 'required'],
            [['body'], 'string'],
            [['parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['slug'], 'string', 'max' => 1024],
            [['title'], 'string', 'max' => 512],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'title' => 'Title',
            'body' => 'Body',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            ];
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticles()
    {
        return $this->hasMany($this->called_class_namespace . '\Article', ['category_id' => 'id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getParent()
    {
        return $this->hasOne($this->called_class_namespace . '\ArticleCategory', ['id' => 'parent_id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
    */
    public function getArticleCategories()
    {
        return $this->hasMany($this->called_class_namespace . '\ArticleCategory', ['parent_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\generated\query\ArticleCategoryQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new \common\models\query\ArticleCategoryQuery(get_called_class());
    }
}
