<?php
/**
 * Copyright (C) $user$, Inc - All Rights Reserved
 *
 *  <other text>
 * @file        Task.php
 * @author      ignatenkovnikita
 * @date        $date$
 */

/**
 * Created by PhpStorm.
 * User: ignatenkovnikita
 * Web Site: http://IgnatenkovNikita.ru
 */

namespace common\models;


use yii\helpers\ArrayHelper;

class Task extends \common\models\generated\models\Task
{

    public function rules()
    {
        // todo this override not correct, because new rules not define
        // change to dynamic
        return [
            [['name', 'status_id'], 'required'],
            [['description'], 'string'],
            [['status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['deadline'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],

            [['name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }




}